package node

import (
	"context"
	"encoding/json"
	"os"
	"time"

	runtime "robomotion/lib/runtime"
)

var (
	ActiveNodes int
	NFactories  []*NodeFactory
)

type NodeFactory struct {
	Node     Node
	NodeName string
}

type INodeProps struct {
	Guid        string
	Name        string
	DelayBefore float32
	DelayAfter  float32
}

type Node struct {
	Props             INodeProps
	OnCreateCallback  func(config []byte) error
	OnMessageCallback func(inMsg []byte) ([]byte, error)
	OnCloseCallback   func() error
}

func NewNodeFactory(node Node, nodeName string) *NodeFactory {
	nf := new(NodeFactory)
	nf.Node = node
	nf.NodeName = nodeName
	NFactories = append(NFactories, nf)
	return nf
}

func (n *NodeFactory) Register() {
	runtime.RegisterNode(n.NodeName, n)
}

func (n *NodeFactory) OnCreate(ctx context.Context, config []byte) error {

	err := json.Unmarshal(config, &n.Node.Props)
	if err != nil {
		return runtime.NewError("Core.ClipBoard.Get.OnCreate", "Config parse error")
	}
	ActiveNodes++

	n.Node.OnCreateCallback(config)
	runtime.CreateNode(n.Node.Props.Guid, n.Node.Props.Name, &n.Node)

	return nil

}

func (n *Node) OnMessage(ctx context.Context, inMsg []byte) (outMsg interface{}, err error) {
	/*
		var msg = make(map[string]interface{})
		err = json.Unmarshal(inMsg, &msg)
		if err != nil {
			err = runtime.NewError("Core.CSV.AppenCSV.OnMessage", "Message parse error")
			return
		}
	*/
	time.Sleep(time.Duration(n.Props.DelayBefore*1000) * time.Millisecond)
	//variable := runtime.GetVariable(n.props.Variable.Scope, n.props.Variable.Name)

	/*actual, e := clipboard.ReadAll()
	if e != nil {
		err = runtime.NewError("Clipboard.Get.ErrGetClipboard", e.Error())
		return
	}

		if variable != nil {
			if e = variable.SetStringValue(actual); e != nil {
				err = runtime.NewError("Clipboard.Get.ErrGetClipboard", e.Error())
				return
			}
		}
	*/

	outMsg, err = n.OnMessageCallback(inMsg)
	if err != nil {
		return
	}
	time.Sleep(time.Duration(n.Props.DelayAfter*1000) * time.Millisecond)
	return
}

func (n *Node) OnClose(ctx context.Context) (err error) {
	err = n.OnCloseCallback()

	ActiveNodes--
	if ActiveNodes == 0 {
		os.Exit(1)
	}

	return
}
