package lib

import (
	"flag"
	"fmt"
	"time"

	"robomotion/lib/logging"
	"robomotion/lib/messaging"
	node "robomotion/lib/node"
	"robomotion/lib/runtime"

	nats "github.com/nats-io/go-nats"

	"github.com/nats-io/gnatsd/server"
)

func Run() (err error) {
	flag.CommandLine.Parse([]string{})
	options := &server.Options{
		Host:           "127.0.0.1",
		Port:           4222,
		NoLog:          false,
		NoSigs:         false,
		MaxControlLine: 256,
	}
	url := fmt.Sprintf("nats://%s:%d", options.Host, options.Port)
	node.ActiveNodes = 0

	conn, err := nats.Connect(url, nats.Timeout(5*time.Second))
	if err != nil {
		return err
	}

	messaging.SetConnection(conn)

	nc := messaging.Connection()
	_, err = nc.Subscribe("wires", runtime.GetWire)
	if err != nil {
		return err
	}

	for _, n := range node.NFactories {
		n.Register()
	}

	logging.DebugInfo("Started")

	ch := make(chan bool, 1)
	<-ch

	return
}
