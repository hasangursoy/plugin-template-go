// source: rpc.proto

/*
Package nrpc is a generated protocol buffer package.

It is generated from these files:
	rpc.proto

It has these top-level messages:
	Error
	Void
	NoRequest
	NoReply
	HeartBeat
*/
package messaging

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import google_protobuf "github.com/golang/protobuf/protoc-gen-go/descriptor"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type SubjectRule int32

const (
	SubjectRule_COPY    SubjectRule = 0
	SubjectRule_TOLOWER SubjectRule = 1
)

var SubjectRule_name = map[int32]string{
	0: "COPY",
	1: "TOLOWER",
}
var SubjectRule_value = map[string]int32{
	"COPY":    0,
	"TOLOWER": 1,
}

func (x SubjectRule) String() string {
	return proto.EnumName(SubjectRule_name, int32(x))
}
func (SubjectRule) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type Error_Type int32

const (
	Error_CLIENT Error_Type = 0
	Error_SERVER Error_Type = 1
	Error_EOS    Error_Type = 3
)

var Error_Type_name = map[int32]string{
	0: "CLIENT",
	1: "SERVER",
	3: "EOS",
}
var Error_Type_value = map[string]int32{
	"CLIENT": 0,
	"SERVER": 1,
	"EOS":    3,
}

func (x Error_Type) String() string {
	return proto.EnumName(Error_Type_name, int32(x))
}
func (Error_Type) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 0} }

type Error struct {
	Type     Error_Type `protobuf:"varint,1,opt,name=type,enum=nrpc.Error_Type" json:"type,omitempty"`
	Message  string     `protobuf:"bytes,2,opt,name=message" json:"message,omitempty"`
	MsgCount uint32     `protobuf:"varint,3,opt,name=msgCount" json:"msgCount,omitempty"`
}

func (m *Error) Reset()                    { *m = Error{} }
func (m *Error) String() string            { return proto.CompactTextString(m) }
func (*Error) ProtoMessage()               {}
func (*Error) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Error) GetType() Error_Type {
	if m != nil {
		return m.Type
	}
	return Error_CLIENT
}

func (m *Error) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *Error) GetMsgCount() uint32 {
	if m != nil {
		return m.MsgCount
	}
	return 0
}

type Void struct {
}

func (m *Void) Reset()                    { *m = Void{} }
func (m *Void) String() string            { return proto.CompactTextString(m) }
func (*Void) ProtoMessage()               {}
func (*Void) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

type NoRequest struct {
}

func (m *NoRequest) Reset()                    { *m = NoRequest{} }
func (m *NoRequest) String() string            { return proto.CompactTextString(m) }
func (*NoRequest) ProtoMessage()               {}
func (*NoRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

type NoReply struct {
}

func (m *NoReply) Reset()                    { *m = NoReply{} }
func (m *NoReply) String() string            { return proto.CompactTextString(m) }
func (*NoReply) ProtoMessage()               {}
func (*NoReply) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

type HeartBeat struct {
	Lastbeat bool `protobuf:"varint,1,opt,name=lastbeat" json:"lastbeat,omitempty"`
}

func (m *HeartBeat) Reset()                    { *m = HeartBeat{} }
func (m *HeartBeat) String() string            { return proto.CompactTextString(m) }
func (*HeartBeat) ProtoMessage()               {}
func (*HeartBeat) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func (m *HeartBeat) GetLastbeat() bool {
	if m != nil {
		return m.Lastbeat
	}
	return false
}

var E_PackageSubject = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.FileOptions)(nil),
	ExtensionType: (*string)(nil),
	Field:         50000,
	Name:          "nrpc.packageSubject",
	Tag:           "bytes,50000,opt,name=packageSubject",
	Filename:      "rpc.proto",
}

var E_PackageSubjectParams = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.FileOptions)(nil),
	ExtensionType: ([]string)(nil),
	Field:         50001,
	Name:          "nrpc.packageSubjectParams",
	Tag:           "bytes,50001,rep,name=packageSubjectParams",
	Filename:      "rpc.proto",
}

var E_ServiceSubjectRule = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.FileOptions)(nil),
	ExtensionType: (*SubjectRule)(nil),
	Field:         50002,
	Name:          "nrpc.serviceSubjectRule",
	Tag:           "varint,50002,opt,name=serviceSubjectRule,enum=nrpc.SubjectRule",
	Filename:      "rpc.proto",
}

var E_MethodSubjectRule = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.FileOptions)(nil),
	ExtensionType: (*SubjectRule)(nil),
	Field:         50003,
	Name:          "nrpc.methodSubjectRule",
	Tag:           "varint,50003,opt,name=methodSubjectRule,enum=nrpc.SubjectRule",
	Filename:      "rpc.proto",
}

var E_ServiceSubject = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.ServiceOptions)(nil),
	ExtensionType: (*string)(nil),
	Field:         51000,
	Name:          "nrpc.serviceSubject",
	Tag:           "bytes,51000,opt,name=serviceSubject",
	Filename:      "rpc.proto",
}

var E_ServiceSubjectParams = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.ServiceOptions)(nil),
	ExtensionType: ([]string)(nil),
	Field:         51001,
	Name:          "nrpc.serviceSubjectParams",
	Tag:           "bytes,51001,rep,name=serviceSubjectParams",
	Filename:      "rpc.proto",
}

var E_MethodSubject = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.MethodOptions)(nil),
	ExtensionType: (*string)(nil),
	Field:         52000,
	Name:          "nrpc.methodSubject",
	Tag:           "bytes,52000,opt,name=methodSubject",
	Filename:      "rpc.proto",
}

var E_MethodSubjectParams = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.MethodOptions)(nil),
	ExtensionType: ([]string)(nil),
	Field:         52001,
	Name:          "nrpc.methodSubjectParams",
	Tag:           "bytes,52001,rep,name=methodSubjectParams",
	Filename:      "rpc.proto",
}

var E_StreamedReply = &proto.ExtensionDesc{
	ExtendedType:  (*google_protobuf.MethodOptions)(nil),
	ExtensionType: (*bool)(nil),
	Field:         52002,
	Name:          "nrpc.streamedReply",
	Tag:           "varint,52002,opt,name=streamedReply",
	Filename:      "rpc.proto",
}

func init() {
	proto.RegisterType((*Error)(nil), "nrpc.Error")
	proto.RegisterType((*Void)(nil), "nrpc.Void")
	proto.RegisterType((*NoRequest)(nil), "nrpc.NoRequest")
	proto.RegisterType((*NoReply)(nil), "nrpc.NoReply")
	proto.RegisterType((*HeartBeat)(nil), "nrpc.HeartBeat")
	proto.RegisterEnum("nrpc.SubjectRule", SubjectRule_name, SubjectRule_value)
	proto.RegisterEnum("nrpc.Error_Type", Error_Type_name, Error_Type_value)
	proto.RegisterExtension(E_PackageSubject)
	proto.RegisterExtension(E_PackageSubjectParams)
	proto.RegisterExtension(E_ServiceSubjectRule)
	proto.RegisterExtension(E_MethodSubjectRule)
	proto.RegisterExtension(E_ServiceSubject)
	proto.RegisterExtension(E_ServiceSubjectParams)
	proto.RegisterExtension(E_MethodSubject)
	proto.RegisterExtension(E_MethodSubjectParams)
	proto.RegisterExtension(E_StreamedReply)
}

func init() { proto.RegisterFile("rpc.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 500 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x92, 0xcf, 0x6e, 0xd3, 0x40,
	0x10, 0xc6, 0x6b, 0x1c, 0x9a, 0x64, 0xa2, 0x46, 0xe9, 0xd2, 0x83, 0x15, 0x21, 0x88, 0xac, 0x4a,
	0x8d, 0x90, 0x70, 0xa4, 0x72, 0xf3, 0xb1, 0x51, 0x22, 0x2a, 0x95, 0xba, 0xda, 0x84, 0x22, 0xb8,
	0xa0, 0xb5, 0x33, 0xb8, 0x06, 0x3b, 0x6b, 0x76, 0xd7, 0x48, 0x79, 0x81, 0x9e, 0x50, 0x8f, 0x3d,
	0x03, 0x4f, 0x01, 0x6f, 0xc1, 0x9f, 0x17, 0x42, 0x5e, 0x3b, 0x55, 0x4d, 0x02, 0xe1, 0xe6, 0x99,
	0x9d, 0xef, 0xf7, 0x7d, 0x3b, 0x6b, 0x80, 0xb9, 0x48, 0x03, 0x27, 0x15, 0x5c, 0x71, 0x52, 0xcb,
	0xbf, 0xbb, 0xbd, 0x90, 0xf3, 0x30, 0xc6, 0x81, 0xee, 0xf9, 0xd9, 0x9b, 0xc1, 0x0c, 0x65, 0x20,
	0xa2, 0x54, 0x71, 0x51, 0xcc, 0xd9, 0x1f, 0x0d, 0xb8, 0x3b, 0x12, 0x82, 0x0b, 0xb2, 0x0f, 0x35,
	0xb5, 0x48, 0xd1, 0x32, 0x7a, 0x46, 0xbf, 0x7d, 0xd8, 0x71, 0x34, 0x4c, 0x1f, 0x39, 0xd3, 0x45,
	0x8a, 0x54, 0x9f, 0x12, 0x0b, 0xea, 0x09, 0x4a, 0xc9, 0x42, 0xb4, 0xee, 0xf4, 0x8c, 0x7e, 0x93,
	0x2e, 0x4b, 0xd2, 0x85, 0x46, 0x22, 0xc3, 0x21, 0xcf, 0xe6, 0xca, 0x32, 0x7b, 0x46, 0x7f, 0x87,
	0xde, 0xd4, 0xf6, 0x01, 0xd4, 0x72, 0x06, 0x01, 0xd8, 0x1e, 0x9e, 0x1c, 0x8f, 0x4e, 0xa7, 0x9d,
	0xad, 0xfc, 0x7b, 0x32, 0xa2, 0xe7, 0x23, 0xda, 0x31, 0x48, 0x1d, 0xcc, 0x91, 0x37, 0xe9, 0x98,
	0xf6, 0x36, 0xd4, 0xce, 0x79, 0x34, 0xb3, 0x5b, 0xd0, 0x3c, 0xe5, 0x14, 0xdf, 0x67, 0x28, 0x95,
	0xdd, 0x84, 0x7a, 0x5e, 0xa4, 0xf1, 0xc2, 0x3e, 0x80, 0xe6, 0x53, 0x64, 0x42, 0x1d, 0x21, 0x53,
	0xb9, 0x63, 0xcc, 0xa4, 0xf2, 0x91, 0x29, 0x9d, 0xba, 0x41, 0x6f, 0xea, 0x47, 0xfb, 0xd0, 0x9a,
	0x64, 0xfe, 0x5b, 0x0c, 0x14, 0xcd, 0x62, 0x24, 0x0d, 0xa8, 0x0d, 0xbd, 0xb3, 0x97, 0x9d, 0x2d,
	0xd2, 0x82, 0xfa, 0xd4, 0x3b, 0xf1, 0x5e, 0xe4, 0xbe, 0xee, 0x18, 0xda, 0x29, 0x0b, 0xde, 0xb1,
	0x10, 0xcb, 0x61, 0x72, 0xdf, 0x29, 0x56, 0xe6, 0x2c, 0x57, 0xe6, 0x8c, 0xa3, 0x18, 0xbd, 0x54,
	0x45, 0x7c, 0x2e, 0xad, 0xef, 0x97, 0xa6, 0xbe, 0xf4, 0x1f, 0x2a, 0x97, 0xc2, 0x5e, 0xb5, 0x73,
	0xc6, 0x04, 0x4b, 0xe4, 0x06, 0xda, 0x8f, 0x4b, 0xb3, 0x67, 0xf6, 0x9b, 0x74, 0xad, 0xd6, 0x65,
	0x40, 0x24, 0x8a, 0x0f, 0x51, 0x80, 0xb7, 0x2f, 0xf2, 0x6f, 0xe2, 0x4f, 0x9d, 0xaf, 0x7d, 0xb8,
	0x5b, 0xbc, 0xde, 0x2d, 0x21, 0x5d, 0x03, 0x73, 0x5f, 0xc3, 0x6e, 0x82, 0xea, 0x82, 0xcf, 0xfe,
	0xdf, 0xe1, 0xd7, 0xdf, 0x1d, 0x56, 0x59, 0xee, 0x31, 0xb4, 0xab, 0xb6, 0xe4, 0xe1, 0x0a, 0x7d,
	0x52, 0x0c, 0x2c, 0x0d, 0xbe, 0x5e, 0x95, 0x2b, 0xae, 0x0a, 0xdd, 0xe7, 0xb0, 0x57, 0xed, 0x94,
	0x2b, 0xde, 0x08, 0xfc, 0x76, 0x55, 0x6e, 0x79, 0x9d, 0xdc, 0x1d, 0xc3, 0x4e, 0x25, 0x36, 0x79,
	0xb0, 0xc2, 0x7b, 0xa6, 0xcf, 0x97, 0xb8, 0x4f, 0xd7, 0x45, 0xbe, 0xaa, 0xcc, 0xa5, 0x70, 0xaf,
	0xd2, 0x28, 0xd3, 0x6d, 0xa2, 0x7d, 0xbe, 0x2e, 0xc2, 0xad, 0x13, 0xe7, 0xd9, 0xa4, 0x12, 0xc8,
	0x12, 0x9c, 0xe9, 0xbf, 0x7f, 0x23, 0xed, 0x8b, 0xce, 0xd6, 0xa0, 0x55, 0xd9, 0x51, 0xf7, 0x95,
	0x15, 0x46, 0xea, 0x22, 0xf3, 0x9d, 0x80, 0x27, 0x83, 0x39, 0x53, 0xf2, 0xb1, 0x48, 0x83, 0x41,
	0xfe, 0x80, 0xfe, 0xb6, 0x46, 0x3d, 0xf9, 0x1d, 0x00, 0x00, 0xff, 0xff, 0x36, 0xdc, 0x4d, 0xaf,
	0x3c, 0x04, 0x00, 0x00,
}
