package messaging

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"runtime/debug"
	"strings"
	"time"

	"github.com/nats-io/gnatsd/server"
	nats "github.com/nats-io/go-nats"
)

var conn *nats.Conn

func ConnectSelf(opts *server.Options) (*nats.Conn, error) {
	var err error
	url := fmt.Sprintf("nats://%s:%d", opts.Host, opts.Port)
	conn, err = nats.Connect(url, nats.Timeout(5*time.Second))
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func Connection() *nats.Conn {
	return conn
}

type NatsConn interface {
	Publish(subj string, data []byte) error
	PublishRequest(subj, reply string, data []byte) error
	Request(subj string, data []byte, timeout time.Duration) (*nats.Msg, error)

	ChanSubscribe(subj string, ch chan *nats.Msg) (*nats.Subscription, error)
	Subscribe(subj string, handler nats.MsgHandler) (*nats.Subscription, error)
	SubscribeSync(subj string) (*nats.Subscription, error)
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s error: %s", Error_Type_name[int32(e.Type)], e.Message)
}

func Send(subject string, data []byte) error {

	nc := Connection()
	err := nc.Publish(subject, data)
	if err != nil {
		log.Printf("nrpc: nats publish failed: %v", err)
	}
	return err

}

func Unmarshal(data []byte, req interface{}) error {
	stype := reflect.ValueOf(req).Elem()
	v := stype.FieldByName("Payload")
	i := stype.FieldByName("Id")
	if v.IsValid() && v.Type() == reflect.TypeOf([]uint8{}) {
		var m map[string]interface{}
		json.Unmarshal(data, &m)
		bs, ok := m["payload"]
		if ok {
			d, err := json.Marshal(bs)
			if err != nil {
				return err
			}
			v.SetBytes(d)
		}
		if i.IsValid() {
			if id, ok := m["id"]; ok {
				i.SetString(id.(string))
			}
		}
		return nil
	}

	return json.Unmarshal(data, &req)
}

func Marshal(v interface{}) ([]byte, error) {
	var (
		data []byte
		err  error
	)
	if bs, ok := v.([]byte); ok {
		data = bs
	} else {
		data, err = json.Marshal(v)
		if err != nil {
			return nil, err
		}
	}
	return data, err
}

// CaptureErrors runs a handler and convert error and panics into proper Error
func CaptureErrors(fn func() (interface{}, error)) (msg interface{}, replyError *Error) {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("Caught panic: %s\n%s", r, debug.Stack())
			replyError = &Error{
				Type:    Error_SERVER,
				Message: fmt.Sprint(r),
			}
		}
	}()
	var err error
	msg, err = fn()
	if err != nil {
		var ok bool
		if replyError, ok = err.(*Error); !ok {
			replyError = &Error{
				Type:    Error_CLIENT,
				Message: err.Error(),
			}
		}
	}
	return
}

func ParseSubject(subject string) (guid string, method string) {
	if strings.HasPrefix(subject, "Error.") {
		method = "OnMessage"
		return
	}
	tokens := strings.Split(subject, ".")
	//	name = tokens[0]
	guid = tokens[0]
	method = tokens[1]
	return
}

func SetConnection(connection *nats.Conn) *nats.Conn {
	conn = connection
	return conn
}
