package logging

import (
	"os"

	log "github.com/sirupsen/logrus"
)

func initConsoleLogger() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.InfoLevel)
	log.SetOutput(os.Stdout)
}

func getConsoleLogger(status string, fields ...LogField) *log.Entry {
	lf := log.Fields{}
	if status != "" {
		lf["status"] = status
	}
	for _, f := range fields {
		lf[f.Name] = f.Value
	}
	return log.WithFields(lf)
}
