package logging

import "fmt"

const (
	StatusConnecting       = "connecting"
	StatusConnected        = "connected"
	StatusFailed           = "failed"
	StatusDisconnected     = "disconnected"
	StatusStarted          = "started"
	StatusStopped          = "stopped"
	StatusWaitingInput     = "waiting_input"
	StatusWaitingSelection = "waiting_selection"

	ErrorPrefix = "[ERROR]"
	InfoPrefix  = "[INFO]"
	LogPrefix   = "[LOG]"
)

func init() {
	initConsoleLogger()
}

type LogField struct {
	Name  string
	Value interface{}
}

func Info(status string, msg string, fields ...LogField) {
	getConsoleLogger(status, fields...).Info(msg)
	GetFileLogger(status, fields...).Info(msg)
}

func Warn(status string, msg string, fields ...LogField) {
	getConsoleLogger(status, fields...).Warn(msg)
	GetFileLogger(status, fields...).Warn(msg)
}

func Error(status string, msg string, fields ...LogField) {
	getConsoleLogger(status, fields...).Error(msg)
	GetFileLogger(status, fields...).Error(msg)
}

func Fatal(status string, msg string, fields ...LogField) {
	getConsoleLogger(status, fields...).Fatal(msg)
	GetFileLogger(status, fields...).Fatal(msg)
}

func DebugError(format string, args ...interface{}) {
	debug(ErrorPrefix, format, args...)
}

func DebugInfo(format string, args ...interface{}) {
	debug(InfoPrefix, format, args...)
}

func DebugLog(format string, args ...interface{}) {
	debug(LogPrefix, format, args...)
}

func debug(prefix, format string, args ...interface{}) {
	fmt.Printf("%s %s\n", prefix, fmt.Sprintf(format, args...))
}
