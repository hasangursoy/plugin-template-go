package logging

import (
	"os"
	"runtime"

	"robomotion/lib/logging/glog"
)

func InitFileLogger(logDir string) {
	if logDir == "" {
		logDir = getLogDir()
		os.MkdirAll(logDir, os.ModePerm)
	}
	glog.Init("robomotion-desktop", logDir)
}

func GetFileLogger(status string, fields ...LogField) *FileLogger {
	return &FileLogger{
		Status: status,
		Fields: fields,
	}
}

type FileLogger struct {
	Status string
	Fields []LogField
}

func (f *FileLogger) Info(msg string) {
	glog.Info(msg)
}

func (f *FileLogger) Warn(msg string) {
	glog.Warning(msg)
}

func (f *FileLogger) Error(msg string) {
	glog.Error(msg)
}

func (f *FileLogger) Fatal(msg string) {
	glog.Fatal(msg)
}

func getLogDir() string {
	var logDir string

	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		logDir = home + "\\AppData\\Robomotion\\logs"
	} else if runtime.GOOS == "darwin" {
		home := os.Getenv("HOME")
		logDir = home + "/Library/Logs/Robomotion"
	} else if runtime.GOOS == "linux" {
		home := os.Getenv("HOME")
		logDir = home + "/.config/Robomotion/logs"
	}

	//fmt.Printf("LogDIR: %s\n", logDir)
	return logDir
}
