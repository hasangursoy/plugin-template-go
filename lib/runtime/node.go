package runtime

import (
	"context"
	"fmt"

	nats "github.com/nats-io/go-nats"

	"robomotion/lib/logging"
	"robomotion/lib/messaging"
)

type Node interface {
	OnMessage(ctx context.Context, inMsg []byte) (outMsg interface{}, err error)
	OnClose(ctx context.Context) error
}

type nodeHandler struct {
	name         string
	guid         string
	subscription *nats.Subscription
	handler      Node
}

func NewNode(guid string, name string, node Node) *nodeHandler {
	return &nodeHandler{
		name:    name,
		guid:    guid,
		handler: node,
	}
}

func (h *nodeHandler) SetSubscription(subscription *nats.Subscription) {
	h.subscription = subscription
}

func (h *nodeHandler) Name() string {
	return h.name
}

func (h *nodeHandler) GUID() string {
	return h.guid
}

func (h *nodeHandler) Subject() string {
	return h.GUID() + "." + ">"
}

func (h *nodeHandler) Handler(msg *nats.Msg) {
	guid, method := messaging.ParseSubject(msg.Subject)
	// Catch node has empty guid
	if guid == "" {
		guid = h.GUID()
	}
	//log.Infof("nodeHandler.%s", msg.Subject)
	var replyError *messaging.Error
	var resp interface{}
	ctx := context.TODO()
	switch method {
	case "OnClose":
		logging.DebugLog("%s on close", h.Name())
		h.subscription.Unsubscribe()
		resp, replyError = messaging.CaptureErrors(
			func() (interface{}, error) {
				err := h.handler.OnClose(ctx)
				if err != nil {
					return nil, err
				}
				return nil, err
			})
	case "OnMessage":
		EmitFlowEvent(guid, FlowEventReceive)
		resp, replyError = messaging.CaptureErrors(
			func() (interface{}, error) {
				logging.DebugLog("%s on message started", h.Name())
				innerResp, err := h.handler.OnMessage(ctx, msg.Data)
				if err != nil {
					//EmitError(guid, err)
					return nil, err
				}
				if innerResp == nil {
					innerResp = msg.Data
				}
				logging.DebugLog("%s on message finished\n", h.Name())
				return innerResp, err
			})
	default:
		logging.DebugError("%s.nodeHandler: unknown node %q", guid, guid)
		replyError = &messaging.Error{
			Type:    messaging.Error_CLIENT,
			Message: "unknown node: " + guid,
		}
	}

	if replyError == nil {
		if payload, ok := resp.([]byte); ok {
			EmitOutput(guid, payload, 0)
			return
		}
		if payloads, ok := resp.([][]byte); ok {
			for port, payload := range payloads {
				if payload != nil {
					EmitOutput(guid, payload, port)
				}
			}
			return
		}
	} else {
		logging.DebugError("%s.%s error: %s", guid, h.Name(), replyError.Message)
		EmitError(guid, NewError(fmt.Sprintf("%s Err", h.Name()), replyError.Message))
	}
}
