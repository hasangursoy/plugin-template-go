package runtime

import (
	"encoding/json"
	"fmt"
	"time"

	"robomotion/lib/messaging"

	log "github.com/sirupsen/logrus"
)

type Event int

const (
	CreateEvent Event = 0
	InputEvent  Event = 1
	OutputEvent Event = 2
	ErrorEvent  Event = 3
	CloseEvent  Event = 4
	LogEvent    Event = 5
	FlowEvent   Event = 6
	DebugEvent  Event = 7
)

const (
	FlowEventSend    = "send"
	FlowEventReceive = "receive"
	FlowEventError   = "error"
	FlowEventStart   = "start"
	FlowEventStop    = "stop"
)

type eventData struct {
	MessageID string `json:"messageId"`
	Type      string `json:"type"`
	NodeID    string `json:"nodeid"`
	Payload   string `json:"payload"`
	Timestamp int64  `json:"timestamp"`
}

type logData struct {
	Type      string `json:"type"`
	Level     string `json:"level"`
	Text      string `json:"text"`
	Timestamp int64  `json:"timestamp"`
}

type errData struct {
	Payload *Error `json:"payload"`
}

type EventPusher interface {
	Push(data []byte)
}

var (
	eventPusher EventPusher
)

func SetEventPusher(p EventPusher) {
	eventPusher = p
}

func EmitFlowEvent(guid string, name string) {
	t := time.Now().Local()
	e := &eventData{
		Type:      "event",
		NodeID:    guid,
		Payload:   name,
		Timestamp: t.UnixNano() / 1000000,
	}
	data, err := json.Marshal(e)
	if err != nil {
		fmt.Printf("EmitInput error: %+v", err)
		return
	}
	EmitEvent(FlowEvent, guid, data)
}

func Debug(guid string, msgId string, msg interface{}) {
	data, err := messaging.Marshal(msg)
	if err != nil {
		fmt.Printf("Log error: %+v", err)
	}
	t := time.Now().Local()
	e := &eventData{
		Type:      "debug",
		MessageID: msgId,
		NodeID:    guid,
		Payload:   string(data),
		Timestamp: t.UnixNano() / 1000000,
	}

	d, err := json.Marshal(e)
	if err != nil {
		log.Errorf("Log error: %+v", err)
	}
	EmitEvent(DebugEvent, guid, d)
}

func Log(level string, text string) {
	t := time.Now().Local()
	l := &logData{
		Type:      "log",
		Level:     level,
		Text:      text,
		Timestamp: t.UnixNano() / 1000000,
	}

	d, err := json.Marshal(l)
	if err != nil {
		log.Errorf("Log error: %+v", err)
	}
	EmitEvent(LogEvent, "", d)
}

func EmitInput(guid string, input interface{}) {
	data, err := messaging.Marshal(input)
	if err != nil {
		fmt.Printf("EmitInput error: %+v", err)
	}
	EmitEvent(InputEvent, guid, data)
}

func EmitOutput(guid string, output interface{}, port int) {
	data, err := messaging.Marshal(output)
	if err != nil {
		fmt.Printf("EmitOutput error: %+v", err)
	}

	wires := nodeMap[guid]

	if len(wires) > 0 {
		for _, out := range wires[port] {
			messaging.Send(out+".OnMessage", data)
		}
	}
	EmitEvent(OutputEvent, guid, data)
}

func EmitError(guid string, in interface{}) {

	// send to Error. subject for Catch node
	ed := &errData{
		Payload: in.(*Error),
	}
	data, err := messaging.Marshal(ed)
	if err != nil {
		fmt.Printf("ErrorData error: %+v", err)
		return
	}
	messaging.Send("Error."+guid, data)

	// prepare for websocket info
	data, err = messaging.Marshal(in.(*Error))
	if err != nil {
		fmt.Printf("ErrorData error: %+v", err)
		return
	}
	t := time.Now().Local()
	l := &eventData{
		Type:      "error",
		NodeID:    guid,
		Payload:   string(data),
		Timestamp: t.UnixNano() / 1000000,
	}
	data, err = json.Marshal(l)
	if err != nil {
		fmt.Printf("EmitInput error: %+v", err)
		return
	}
	EmitEvent(ErrorEvent, guid, data)

}

func EmitEvent(event Event, subject string, data []byte) {

	switch event {
	case CreateEvent:
		//log.Infof("Create Event: %s", subject)
		messaging.Send(subject, data)
		break
	case InputEvent:
		//log.Infof("Input Event: %s", subject)
		messaging.Send(subject+".OnMessage", data)
		break
	case OutputEvent:
		//log.Infof("Output Event: %s", subject)
		EmitFlowEvent(subject, FlowEventSend)
		break
	case FlowEvent:
		//log.Infof("Log Event: %s", subject)
		if eventPusher != nil {
			eventPusher.Push(data)
		}
		messaging.Send("FlowEvent", data)
	case DebugEvent, LogEvent:
		//log.Infof("Log Event: %s", subject)
		if eventPusher != nil {
			eventPusher.Push(data)
		}
		break
	case ErrorEvent:
		//log.Infof("Error Event: %s", subject)
		EmitFlowEvent(subject, FlowEventError)
		if eventPusher != nil {
			eventPusher.Push(data)
		}
		break
	case CloseEvent:
		for guid := range nodeMap {
			subject := guid + ".OnClose"
			//	log.Infof("Close Event: %s", subject)
			messaging.Send(subject, []byte(""))
		}
		nodeMap = make(map[string][][]string)
		break
	}

}
