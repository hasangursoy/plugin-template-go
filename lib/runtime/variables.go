package runtime

const (
	ScopeGlobal  = "Global"
	ScopeFlow    = "Flow"
	ScopeLocal   = "Local"
	ScopeMessage = "Message"
)

const (
	TodoFlowGUID = "X"
)

var (
	global map[string]*Variable            // global -> variable name -> variable
	flow   map[string]map[string]*Variable // flow -> flow guid -> variable name -> variable
)

func ResetVariables() {
	global = make(map[string]*Variable)
	flow = make(map[string]map[string]*Variable)
}

func AddGlobalVariable(name string, _type string, value interface{}) {
	v := &Variable{
		Name:  name,
		Type:  _type,
		Value: value,
	}
	global[v.Name] = v
}

func AddFlowVariable(flowId string, name string, _type string, value interface{}) {
	v := &Variable{
		Name:  name,
		Type:  _type,
		Value: value,
	}
	_, exists := flow[flowId]
	if !exists {
		flow[flowId] = make(map[string]*Variable)
	}
	flow[flowId][v.Name] = v
}

func GetVariable(scope string, name string) *Variable {
	if scope == ScopeGlobal {
		v, _ := global[name]
		return v
	}
	if scope == ScopeFlow {
		if f, exists := flow[TodoFlowGUID]; exists {
			v, _ := f[name]
			return v
		}
	}
	return nil
}

/*
func GetGlobalsJson() []byte {

}

func GetFlowJson(guid string) []byte {

}
*/
