package runtime

import (
	"fmt"
	"sync"
)

const (
	VariableTypeString  = "String"
	VariableTypeInteger = "Integer"
	VariableTypeBoolean = "Boolean"
	VariableTypeDouble  = "Double"
	VariableTypeArray   = "Array"
)

type Variable struct {
	Name  string
	Type  string
	Value interface{}
	mutex sync.Mutex
}

func (v *Variable) GetIntValue() (int, error) {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeInteger {
		return 0, fmt.Errorf("Variable type is not Integer")
	}
	value, ok := v.Value.(int)
	if !ok {
		return 0, fmt.Errorf("Variable integer conversion error")
	}
	return value, nil
}

func (v *Variable) SetIntValue(value int) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeInteger {
		return fmt.Errorf("Variable type is not Integer")
	}
	v.Value = value
	return nil
}

func (v *Variable) GetFloatValue() (float64, error) {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeDouble {
		return 0, fmt.Errorf("Variable type is not Double")
	}
	value, ok := v.Value.(float64)
	if !ok {
		return 0, fmt.Errorf("Variable double conversion error")
	}
	return value, nil
}

func (v *Variable) SetFloatValue(value float64) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeDouble {
		return fmt.Errorf("Variable type is not Double")
	}
	v.Value = value
	return nil
}

func (v *Variable) GetStringValue() (string, error) {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeString {
		return "", fmt.Errorf("Variable type is not String")
	}
	value, ok := v.Value.(string)
	if !ok {
		return "", fmt.Errorf("Variable string conversion error")
	}
	return value, nil
}

func (v *Variable) SetStringValue(value string) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeString {
		return fmt.Errorf("Variable type is not String")
	}
	v.Value = value
	return nil
}

func (v *Variable) GetBoolValue() (bool, error) {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeBoolean {
		return false, fmt.Errorf("Variable type is not Boolean")
	}
	value, ok := v.Value.(bool)
	if !ok {
		return false, fmt.Errorf("Variable Boolean conversion error")
	}
	return value, nil
}

func (v *Variable) SetBoolValue(value bool) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeBoolean {
		return fmt.Errorf("Variable type is not Boolean")
	}
	v.Value = value
	return nil
}

func (v *Variable) GetArrayValue() ([]interface{}, error) {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeArray {
		return nil, fmt.Errorf("Variable type is not Array")
	}
	value, ok := v.Value.([]interface{})
	if !ok {
		return nil, fmt.Errorf("Variable Array conversion error")
	}
	return value, nil
}

func (v *Variable) SetArrayValue(value []interface{}) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	if v.Type != VariableTypeArray {
		return fmt.Errorf("Variable type is not Array")
	}
	v.Value = value
	return nil
}

func (v *Variable) SetValue(value interface{}) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	switch value.(type) {
	case int64:
		if v.Type != VariableTypeInteger {
			return fmt.Errorf("Value should be an Integer")
		}
		v.Value = value
	case string:
		if v.Type != VariableTypeString {
			return fmt.Errorf("Value should be a String")
		}
		v.Value = value
	case float64:
		if v.Type == VariableTypeInteger {
			v.Value = int64(value.(float64))
		} else if v.Type != VariableTypeDouble {
			return fmt.Errorf("Value should be a Double")
		} else {
			v.Value = value
		}
	case bool:
		if v.Type != VariableTypeBoolean {
			return fmt.Errorf("Value should be a Bool")
		}
		v.Value = value

	case []string:
	case []int64:
	case []float64:
	case []bool:
	case []interface{}:
		if v.Type != VariableTypeArray {
			return fmt.Errorf("Value should be a Array")
		}
		v.Value = value

	default:
		return fmt.Errorf("Unsupported value type")
	}
	return nil
}
