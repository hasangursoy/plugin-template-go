package runtime

import (
	"context"

	nats "github.com/nats-io/go-nats"
	log "github.com/sirupsen/logrus"
)

type INodeFactory interface {
	OnCreate(ctx context.Context, config []byte) error
}

type nodeFactoryHandler struct {
	handler INodeFactory
}

func newNodeFactory(factory INodeFactory) *nodeFactoryHandler {
	return &nodeFactoryHandler{
		handler: factory,
	}
}

func (h *nodeFactoryHandler) Handler(msg *nats.Msg) {
	if err := h.handler.OnCreate(context.TODO(), msg.Data); err != nil {
		log.Printf("NodeFactory Handler: OnCreate failed: %v", err)
	}
}
