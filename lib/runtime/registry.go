package runtime

import (
	"encoding/json"

	nats "github.com/nats-io/go-nats"
	log "github.com/sirupsen/logrus"

	"robomotion/lib/messaging"
)

type NodeFactory_ interface {
	Handler(msg *nats.Msg)
}

type Node_ interface {
	GUID() string
	Name() string
	Subject() string
	Handler(msg *nats.Msg)
	SetSubscription(subs *nats.Subscription)
}

type Flow struct {
	Guid  string
	Wires [][]string
}

var (
	nodeMap = make(map[string][][]string)
)

func AddNode(namespace string, guid string, wires [][]string, config map[string]interface{}) {
	data, err := json.Marshal(config)
	if err != nil {
		log.Errorf("Config format error: %+v", err)
	}
	EmitEvent(CreateEvent, namespace, data)
	nodeMap[guid] = wires
}

func GetWire(msg *nats.Msg) {
	var flow Flow
	json.Unmarshal(msg.Data, &flow)
	nodeMap[flow.Guid] = flow.Wires
}

func RegisterNode(name string, factory INodeFactory) {
	nc := messaging.Connection()
	nf := newNodeFactory(factory)
	// log.Infof("Factory Subscribe: %s", name)
	_, err := nc.Subscribe(name, nf.Handler)
	if err != nil {
		log.Fatal(err)
	}
}

// TODO: return void idi, catch node u icin eklemek zorunda kaldik
func CreateNode(guid string, name string, node Node) *nodeHandler {
	n := NewNode(guid, name, node)
	nc := messaging.Connection()
	subscription, err := nc.Subscribe(n.Subject(), n.Handler)
	if err != nil {
		log.Fatal(err)
	}
	n.SetSubscription(subscription)
	return n
}
