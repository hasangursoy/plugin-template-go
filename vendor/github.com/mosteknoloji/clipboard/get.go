package clipboard

import (
	"encoding/json"

	runtime "github.com/mosteknoloji/robomotion/runtime"

	Node "github.com/mosteknoloji/robomotion/node"
)

type Variable struct {
	Scope string
	Name  string
}

type NodeProps struct {
	Node.INodeProps
	Variable Variable
}

type Get struct {
	Node     Node.Node
	nodeName string
	Props    NodeProps
}

func (g *Get) Init() {
	g.nodeName = "Plugin.Clipboard.Get"
	g.Node.OnCreateCallback = func(config []byte) error {
		err := json.Unmarshal(config, &g.Props)
		if err != nil {
			return runtime.NewError("Core.ClipBoard.Get.OnCreate", "Config parse error")
		}
		return nil
	}
	g.Node.OnMessageCallback = func(inMsg []byte) string {
		return "foo"
	}
	g.Node.OnCloseCallback = func() {}
	g.Props = NodeProps{}
	Node.NewNodeFactory(g.Node, g.nodeName)
}
