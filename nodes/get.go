package nodes

import (
	"encoding/json"

	Node "robomotion/lib/node"
	runtime "robomotion/lib/runtime"
)

type Variable struct {
	Scope string
	Name  string
}

type NodeProps struct {
	Node.INodeProps
	Variable Variable
}

type Get struct {
	Node     Node.Node
	nodeName string
	Props    NodeProps
}

func (g *Get) Init() {
	g.nodeName = "Plugin.Clipboard.Get"
	g.Node.OnCreateCallback = g.OnCreate
	g.Node.OnMessageCallback = g.OnMessage
	g.Node.OnCloseCallback = g.OnClose
	g.Props = NodeProps{}
	Node.NewNodeFactory(g.Node, g.nodeName)
}

func (g *Get) OnCreate(config []byte) error {
	err := json.Unmarshal(config, &g.Props)
	if err != nil {
		return runtime.NewError("Plugins.Clipboard.Get.OnCreate", "Config parse error")
	}
	return nil
}

func (g *Get) OnMessage(inMsg []byte) ([]byte, error) {
	var msg = make(map[string]interface{})
	err := json.Unmarshal(inMsg, &msg)
	if err != nil {
		err = runtime.NewError("Plugins.Clipboard.OnMessage", "Message parse error")
		return nil, err
	}

	msg["payload"] = "foo"
	data, err := json.Marshal(msg)
	if err != nil {
		err = runtime.NewError("Plugins.Clipboard.Get.OutputMarshal", err.Error())
		return nil, err
	}

	return data, nil
}

func (g *Get) OnClose() error {
	return nil
}
