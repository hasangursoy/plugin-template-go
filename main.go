package main

import (
	nodes "robomotion/nodes"

	robo "robomotion/lib"
	"robomotion/lib/logging"
	node "robomotion/lib/node"
)

func main() {
	(&nodes.Get{}).Init()

	err := robo.Run()
	if err != nil {
		for _, nf := range node.NFactories {
			guid := nf.Node.Props.Guid
			logging.DebugError("%s.Plugin run failed: %+v", guid, err)
		}
	}
}
